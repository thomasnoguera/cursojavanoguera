package Modulo4;

public class Ejercicio20 {

	public static void main(String[] args) {
		short c=0;
		int 		vmax =  (int)-1-(int)Math.pow(2 , 31);
		int 		vmin = (int)Math.pow(2 , 31);
		do {
			var num = Math.random();
			int num_al= (int) (num*1000);
			System.out.println(num_al);
			if (num_al<vmin) vmin=num_al;
			if (num_al>vmax) vmax=num_al;
			c++;	
			} while (c < 10);
		System.out.println("El valor maximo de los anteriores numeros es "+vmax+" y el menor es "+vmin);
	}

}
