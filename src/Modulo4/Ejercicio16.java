package Modulo4;

import java.util.Scanner;

public class Ejercicio16 {

	public static void main(String[] args) {
		System.out.println("Ingrese el valor del que quiere saber su tabla de multiplicaciones");
		int valortabla=extracted().nextInt();
		int c=1;
		while (c<11) {
			System.out.println(valortabla+"x"+c+"\t=\t"+(valortabla*c));
			c++;
		}
	}
	public static Scanner extracted() {
		return new Scanner(System.in);
	}

}
