package Modulo4;

import java.util.Scanner;

public class Ejercicio21 {

	public static void main(String[] args) {
		System.out.println("Ingresar la catgoria (A o B o c), la antiguedad y el sueldo base en ese orden");
		char categoria=extracted().next().charAt(0);
		int antiguedad=extracted().nextInt();
		Double sueldo=extracted().nextDouble();
		if (antiguedad<6) {sueldo=sueldo*1.05;}
		else if(antiguedad<11) {sueldo=sueldo*1.1;}
		else {sueldo=sueldo*1.1;}
		switch (categoria) {
			case 'A' :
				System.out.println("Su sueldo es de $"+(sueldo+1000));
				break;
			case 'B' :
				System.out.println("Su sueldo es de $"+(sueldo+2000));
				break;
			case 'C' :
				System.out.println("Su sueldo es de $"+(sueldo+3000));
				break;
		}
		
	}
	public static Scanner extracted() {
		return new Scanner(System.in);
	}

}
