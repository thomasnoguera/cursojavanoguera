package Modulo4;

import java.util.Scanner;

public class Ejercicio17 {

	public static void main(String[] args) {
		System.out.println("Ingrese el valor del que quiere saber su tabla de multiplicaciones");
		int valortabla=extracted().nextInt();
		int c=1;
		int suma = 0;
		int vactual;
		while (c<11) {
			System.out.println(valortabla+"x"+c+"\t=\t"+(valortabla*c));
			vactual=valortabla*c;
			if ( vactual % 2 == 0 ) {suma = suma+valortabla*c;};
			c++;
		}
		System.out.println("La suma de todos los valores pares es " +  suma);
	}
	public static Scanner extracted() {
		return new Scanner(System.in);
	}

}
