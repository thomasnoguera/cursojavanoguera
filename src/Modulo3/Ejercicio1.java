package Modulo3;

import java.util.Scanner;

public class Ejercicio1 {
	
	public static Scanner extracted() {return new Scanner(System.in);}

	public static void main(String[] args) {
		System.out.println("Ingresar la primera de las 3 notas");
		float nota1=extracted().nextFloat();
		System.out.println("Ingresar la segunda nota");
		Float nota2=extracted().nextFloat();
		System.out.println("Ingresar la tercera nota");
		float nota3=extracted().nextFloat();
		float promedio= (nota1 + nota2 + nota3)/3; 
		if (promedio<7) {
			System.out.println("Tu promedio es "+promedio+" por lo que estas reprobado.");
		}else{
			System.out.println("Tu promedio es "+promedio+" por lo que estas aprobado.");
		}
		
		}

		
}
