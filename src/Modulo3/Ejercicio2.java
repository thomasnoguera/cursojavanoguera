package Modulo3;

import java.util.Scanner;

public class Ejercicio2 {

	public static void main(String[] args) {
		System.out.println("Ingresar un numero cualquiera");
		int num=extracted().nextInt();
		int modulo=num % 2;
		if (modulo==0) { System.out.println("El numero "+num+" es par.");} 
		else {System.out.println("El numero "+num+" es inpar.");}
	}

	public static Scanner extracted() {
		return new Scanner(System.in);
	}

}
