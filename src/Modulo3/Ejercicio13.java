package Modulo3;

import java.util.Scanner;

public class Ejercicio13 {

	public static void main(String[] args) {
		System.out.println("Ingrese el mes");
		String mes=extracted().nextLine();
		switch (mes){
			case "Enero":
				System.out.println("Enero tiene 30 dias");
				break;
			case "Febrero":
				System.out.println("Febrero tiene 28/29 dias");
				break;
			case "Marzo":
				System.out.println("Marzo tiene 31 dias");
				break;
			case "Abril":
				System.out.println("Abril tiene 30 dias");
				break;
			case "Mayo":
				System.out.println("Mayo tiene 31 dias");
				break;
			case "Junio":
				System.out.println("Junio tiene 30 dias");
				break;
			case "Julio":
				System.out.println("Julio tiene 31 dias");
				break;
			case "Agosto":
				System.out.println("Agosto tiene 31 dias");
				break;
			case "Septiembre":
				System.out.println("Septiembre tiene 30 dias");
				break;
			case "Octubre":
				System.out.println("Octubre tiene 31 dias");
				break;
			case "Noviembre":
				System.out.println("Noviembre tiene 30 dias");
				break;
			case "Diciembre":
				System.out.println("Diciembre tiene 31 dias");
				break;

	}}

	public static Scanner extracted() {
		return new Scanner(System.in);
	}

}
