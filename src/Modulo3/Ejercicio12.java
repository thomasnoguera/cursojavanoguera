package Modulo3;

import java.util.Scanner;

public class Ejercicio12 {

	public static void main(String[] args) {
		System.out.println("Ingrese el numero para saber en que docena esta");
		try (Scanner s1 = new Scanner(System.in)) {
			int d = s1.nextInt();
			if (d<=0) {System.out.println("El numero "+d+" esta fuera de rango");}
			else if (d>0 && d<=12) {System.out.println("El numero esta en la primera docena ");}
			else if (d<=24) {System.out.println("El numero esta en la segunda docena ");}
			else if (d<=36) {System.out.println("El numero esta en la tercera docena ");}
			else {System.out.println("El numero "+d+" esta fuera de rango");}
		}
	}

}
