package Modulo3;

import java.util.Scanner;

public class Ejercicio3 {

	public static void main(String[] args) {
	
		System.out.println("Ingrese el mes");
		Scanner s1= new Scanner(System.in);
		String mes=s1.nextLine();
		if (mes.equals("Febrero")) {System.out.println("Febrero tiene 28/29 dias");}
		else if (mes.equals("Enero" )||mes.equals("Abril")||mes.equals("Junio")||mes.equals("Septiembre")||mes.equals("Noviembre")) {System.out.println(mes +" tiene 30 dias");}
		else if (mes.equals("Marzo" )||mes.equals("Mayo")||mes.equals("Julio")||mes.equals("Agosto")||mes.equals("Octubre")||mes.equals("Diciembre")) {System.out.println(mes +" tiene 31 dias");}
		else {System.out.println(mes+" no es reconocible por el programa. Recuerde ingresar el mes con mayuscula al comienzo");}
	}

}
